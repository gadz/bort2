package com.example.prilojenie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;

public class Settings extends Activity implements OnClickListener {

	private static final String LOG_TAG = null;
	Button btnSync;
	Button btnRestore;
	Button btnErase;
	// переменные для БД
	DB db;
	SimpleCursorAdapter scAdapter;
	Cursor cursor;
	String line1 = "";
	String lineRestore;

    private ProgressDialog dialog;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);

		// найдем View-элементы
		btnSync = (Button) findViewById(R.id.button_sync);
		btnRestore = (Button) findViewById(R.id.button_restore);
		btnErase = (Button) findViewById(R.id.button_erase);

		// присваиваем обработчик кнопкам
		btnSync.setOnClickListener(this);
		btnRestore.setOnClickListener(this);
		btnErase.setOnClickListener(this);

		// открытие бд и создание курсора
		db = new DB(this);
		db.open();

		// получить все данные из таблицы
		cursor = db.getAllData();
		startManagingCursor(cursor);

	}

	@Override
	public void onClick(View v) {
		
		String line = "";

		// TODO Auto-generated method stub

		// Это создаем для создания Json строки из данных прочтенных из бд,
		// сначала создаем объекты
		// и заворачиваем в array
		JSONObject object = new JSONObject();
		JSONArray jsonarr = new JSONArray();
		switch (v.getId()) {
		case R.id.button_sync:

			if (cursor.moveToFirst()) {

				// определяем номера столбцов по имени в выборке
			//	int idColIndex = cursor.getColumnIndex("_id");
				int dateColIndex = cursor.getColumnIndex("date");
				int kmColIndex = cursor.getColumnIndex("km");
				int nameColIndex = cursor.getColumnIndex("name");
				int kolColIndex = cursor.getColumnIndex("kol");
				int cenaColIndex = cursor.getColumnIndex("cena");
				int summColIndex = cursor.getColumnIndex("summ");
				int userColIndex = cursor.getColumnIndex("user");
				
				
				do {
					// получаем значения по номерам столбцов и пишем все в лог
					try {
						object.put("date", cursor.getString(dateColIndex));
						object.put("km", cursor.getString(kmColIndex));
						object.put("name", cursor.getString(nameColIndex));
						object.put("kol", cursor.getString(kolColIndex));
						object.put("cena", cursor.getString(cenaColIndex));
						object.put("summ", cursor.getString(summColIndex));
						object.put("user", cursor.getString(userColIndex));
						
						// Log.d("log_tag","JsonString :" + object.toString());
						line = line + object.toString();
						jsonarr.put(object);
					} catch (Exception je) {

					}
					// Log.d(LOG_TAG,
					// "_id = " + cursor.getInt(idColIndex) +
					// ", date = " + cursor.getString(nameColIndex) +
					// ", km = " + cursor.getString(emailColIndex));
					// переход на следующую строку
					// а если следующей нет (текущая - последняя), то false -
					// выходим из цикла
				} while (cursor.moveToNext());
				line1 = jsonarr.toString();
				//Log.d("log_tag", "JsonString :" + jsonarr.toString());
				new RequestTask().execute("http://vitoelexir.ru/android/posttest.php");
                
			} else
				Log.d(LOG_TAG, "0 rows");
			break;

		case R.id.button_restore:
			new RequestRestore().execute("http://vitoelexir.ru/android/getrestore.php");

			break;
		case R.id.button_erase:

			// Log.d(LOG_TAG, "--- Clear mytable: ---");
			// Стирание всей информации с бд
			int clearCount = db.delAll();
			Log.d(LOG_TAG, "deleted rows count = " + clearCount);
			break;
		}

	}

	
	
	
	
	//начало пост запроса
	// делаем в отдельном потоке Асинк таск
	//тут мы отправляем на сервер содержание БД в виде Json строки
	
	 class RequestTask extends AsyncTask<String, String, String> {

         @Override
         protected String doInBackground(String... params) {

                 try {
                         //создаем запрос на сервер
                         DefaultHttpClient hc = new DefaultHttpClient();
                         ResponseHandler<String> res = new BasicResponseHandler();
                         //он у нас будет посылать post запрос
                         HttpPost postMethod = new HttpPost(params[0]);
                         //будем передавать два параметра
                         List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                         //передаем параметры из наших текстбоксов
                         //логин
                         nameValuePairs.add(new BasicNameValuePair("databd", line1));
                         //пароль
                      //   nameValuePairs.add(new BasicNameValuePair("pass", pass.getText().toString()));
                         //собераем их вместе и посылаем на сервер
                         postMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                         //получаем ответ от сервера
                         String response = hc.execute(postMethod, res);
         				Log.d("log_tag", "response :" + response.toString());

                         //посылаем на вторую активность полученные параметры
                   //      Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                         //то что куда мы будем передавать и что, putExtra(куда, что);
                  //       intent.putExtra(SecondActivity.JsonURL, response.toString());
                  //       startActivity(intent);
                 } catch (Exception e) {
                         System.out.println("Exp=" + e);
                 }
                 return null;
         }

         @Override
         protected void onPostExecute(String result) {

                 dialog.dismiss();
                 super.onPostExecute(result);
         }

         @Override
         protected void onPreExecute() {

                 dialog = new ProgressDialog(Settings.this);
                 dialog.setMessage("Синхронизация...");
                 dialog.setIndeterminate(true);
                 dialog.setCancelable(true);
                 dialog.show();
                 super.onPreExecute();
         }
 }
	 
	 
	 
	 // тут начинаем второй асинк таск для восстановления БД данными из сервера
	 
	 
	 class RequestRestore extends AsyncTask<String, String, String> {

         private final String Tag = null;

		@Override
         protected String doInBackground(String... params) {

               
        		DefaultHttpClient client = new DefaultHttpClient(); // создаем  клиента
    			try {
    				HttpResponse response = client.execute(new HttpGet(params[0])); 
    				// пытаемся  выполнить  GET запрос
    				HttpEntity httpEntity = response.getEntity(); 
    				// получаем ответ  (содержимое  страницы  вывода)
    				lineRestore = EntityUtils.toString(httpEntity, "UTF-8");
    				 Log.d(Tag, lineRestore);

    			} catch (Exception e) {
    				Log.i("Request exception", "Exception: " + e.getMessage()); // Oops
    			}

                 return null;
         }

         @Override
         protected void onPostExecute(String result) {

                 dialog.dismiss();
         		ContentValues cv = new ContentValues();

                 
                 try {
 					// создали читателя json объектов и отдали ему строку -
 					// result
 					JSONObject json = new JSONObject(lineRestore);
 					// дальше находим вход в наш json им является ключевое слово
 					// data
 					JSONArray urls = json.getJSONArray("data");
 					// проходим циклом по всем нашим параметрам
 					for (int i = 0; i < urls.length(); i++) {
 						
 						cv.put("date", urls.getJSONObject(i).getString("date").toString());
 						cv.put("km", urls.getJSONObject(i).getString("km").toString());
 						cv.put("name", urls.getJSONObject(i).getString("name").toString());
 						cv.put("kol", urls.getJSONObject(i).getString("kol").toString());
 						cv.put("cena", urls.getJSONObject(i).getString("cena").toString());
 						cv.put("summ", urls.getJSONObject(i).getString("summ").toString());
 						cv.put("user", urls.getJSONObject(i).getString("user").toString());
 						
 						db.addRec(cv);
 						
 						//	Log.d(LOG_TAG, "row inserted, ID = " + rowID);
					
 					}
 					

 				} catch (JSONException e) {
 					Log.e("log_tag", "Error parsing data " + e.toString());
 				} 
                 
                 
                 
                 super.onPostExecute(result);
         }

         @Override
         protected void onPreExecute() {

                 dialog = new ProgressDialog(Settings.this);
                 dialog.setMessage("Восстановление...");
                 dialog.setIndeterminate(true);
                 dialog.setCancelable(true);
                 dialog.show();
                 super.onPreExecute();
         }
 }	
	 
	 
	 
	 
	
	
	protected void onDestroy() {
		super.onDestroy();
		// Закрытие БД
		db.close();
	}

}
