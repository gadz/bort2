package com.example.prilojenie;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DB {
 
  private static final String DB_NAME = "myDB";
  private static final int DB_VERSION = 1;
  private static final String DB_TABLE = "bort";
 
  public static final String COLUMN_ID = "_id";
  public static final String COLUMN_DATE = "date";
  public static final String COLUMN_KM = "km";
  public static final String COLUMN_NAME = "name";
  public static final String COLUMN_COL = "kol";
  public static final String COLUMN_CENA = "cena";
  public static final String COLUMN_SUMM = "summ";
  public static final String COLUMN_PRIM = "prim";
  public static final String COLUMN_USER = "user";
  private static final String DB_CREATE =
    "create table " + DB_TABLE + "(" +
      COLUMN_ID + " integer primary key autoincrement, " +
      COLUMN_DATE + " text, " +
      COLUMN_KM + " integer, " +
      COLUMN_NAME + " text, " +
      COLUMN_COL + " integer, " +
      COLUMN_CENA + " integer, " +
      COLUMN_SUMM + " integer, " +
      COLUMN_USER + " text " +
      
    ");";
 
  private final Context mCtx;
 
 
  private DBHelper mDBHelper;
  private SQLiteDatabase mDB;
 
  public DB(Context ctx) {
    mCtx = ctx;
  }
 
  // открыть подключение

  public void open() {
    mDBHelper = new DBHelper(mCtx, DB_NAME, null, DB_VERSION);
    mDB = mDBHelper.getWritableDatabase();
  }
 
  // закрыть подключение
  public void close() {
    if (mDBHelper!=null) mDBHelper.close();
  }
 
  // получить все данные из таблицы DB_TABLE
  public Cursor getAllData() {
    return mDB.query(DB_TABLE, null, null, null, null, null, null);
  }
 
  // добавить запись в DB_TABLE
  public void addRec(ContentValues cv) {
  // ContentValues cv  = new ContentValues();
  //  cv.put(COLUMN_DATE, txt);
  //  cv.put(COLUMN_KM, img);
    mDB.insert(DB_TABLE, null, cv);
  }
 
//удалить запись из DB_TABLE
  public void delRec(long id) {
    mDB.delete(DB_TABLE, COLUMN_ID + " = " + id, null);
  }
  
//удалить вске записи из DB_TABLE
  public int delAll() {
	   int clearCount = mDB.delete(DB_TABLE, null, null);
	   return clearCount;
  }
  
 
//класс по созданию и управлению БД
  private class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context, String name, CursorFactory factory,
        int version) {
      super(context, name, factory, version);
    }

    // создаем и заполняем БД
    @Override
    public void onCreate(SQLiteDatabase db) {
      db.execSQL(DB_CREATE);
     
   /*   ContentValues cv = new ContentValues();
      for (int i = 1; i < 5; i++) {
        cv.put(COLUMN_TXT, "sometext " + i);
        cv.put(COLUMN_IMG, R.drawable.ic_launcher);
        db.insert(DB_TABLE, null, cv);
      } */
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
  }
}