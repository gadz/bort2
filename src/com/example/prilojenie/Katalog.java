package com.example.prilojenie;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class Katalog extends Activity implements OnClickListener {

	protected static final String Tag = null;
	Button btnRefresh;
	String line;

	private static final String FIRST = "logins";
	private static final String SECOND = "autos";

	private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.katalog);

		// найдем View-элементы
		btnRefresh = (Button) findViewById(R.id.buttonRefresh);

	//	 final  ListView listView = (ListView) findViewById(R.id.listView1);

		// присваиваем обработчик кнопкам
		btnRefresh.setOnClickListener(this);

	//	final ArrayList<HashMap<String, Object>> myBooks = new ArrayList<HashMap<String, Object>>();

	
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// по id определеяем кнопку, вызвавшую этот обработчик
		switch (v.getId()) {
		case R.id.buttonRefresh:
			

			new RequestKatalog().execute("http://vitoelexir.ru/android/bdtest.php");

	


			break;

		}
	}

	// тут начинаем асинк таск для получения списка журналов с сервера

	class RequestKatalog extends AsyncTask<String, String, String> {

		private final String Tag = null;

		@Override
		protected String doInBackground(String... params) {

			DefaultHttpClient client = new DefaultHttpClient(); 
			// создаем  клиента
			try {
				HttpResponse response = client.execute(new HttpGet(params[0])); // пытаемся
				// выполнить   GET  запрос
				HttpEntity httpEntity = response.getEntity(); // получаем
				// ответ  (содержимое  страницы  вывода)
				line = EntityUtils.toString(httpEntity, "UTF-8");
				// Log.d(Tag, line);

			} catch (Exception e) {
				Log.i("Request exception", "Exception: " + e.getMessage()); // Oops
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {

			dialog.dismiss();
			
			final ArrayList<HashMap<String, Object>> myBooks = new ArrayList<HashMap<String, Object>>();
			ListView listView = (ListView) findViewById(R.id.listView1);
			
			try {
				// создали читателя json объектов и отдали ему строку -  result
				JSONObject json = new JSONObject(line);
				// дальше находим вход в наш json им является ключевое слово
				// data
				JSONArray urls = json.getJSONArray("data");
				// проходим циклом по всем нашим параметрам
				for (int i = 0; i < urls.length(); i++) {
					HashMap<String, Object> hm;
					hm = new HashMap<String, Object>();

					// читаем что в себе хранит параметр login
					hm.put(FIRST, urls.getJSONObject(i).getString("login")
							.toString());

					// читаем что в себе хранит параметр auto
					hm.put(SECOND, urls.getJSONObject(i).getString("auto")
							.toString());

					myBooks.add(hm);

				}
				// дальше добавляем полученные параметры в наш адаптер

				SimpleAdapter adapter = new SimpleAdapter(Katalog.this,
						myBooks, R.layout.list, new String[] { FIRST,
								SECOND }, new int[] { R.id.text1,
								R.id.text2 });
				// выводим в листвью
				listView.setAdapter(adapter);

			} catch (JSONException e) {
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
			
			
			
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {

			dialog = new ProgressDialog(Katalog.this);
			dialog.setMessage("Получение...");
			dialog.setIndeterminate(true);
			dialog.setCancelable(true);
			dialog.show();
			super.onPreExecute();
		}
	}

}
