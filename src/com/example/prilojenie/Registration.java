package com.example.prilojenie;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.prilojenie.Katalog.RequestKatalog;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class Registration extends Activity implements OnClickListener {

	protected static final String Tag = null;
	private Button btnRegistration;
	protected String line;
	private Handler h;
	private EditText editTextLogin;
	private EditText editTextAuto;

	private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration);

		// найдем View-элементы

		btnRegistration = (Button) findViewById(R.id.buttonRegistration);

		editTextLogin = (EditText) findViewById(R.id.editTextLogin);
		editTextAuto = (EditText) findViewById(R.id.editTextAuto);
		// присваиваем обработчик кнопкам
		btnRegistration.setOnClickListener(this);

		

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		final String login = editTextLogin.getText().toString();
		final String auto = editTextAuto.getText().toString();

		// TODO Auto-generated method stub
		// по id определеяем кнопку, вызвавшую этот обработчик
		switch (v.getId()) {
		case R.id.buttonRegistration:
			// кнопка ОК
			// tvOut.setText("Нажата кнопка ОК");

			new RequestRegistration().execute("http://vitoelexir.ru/android/adduser.php?var1="+ login + "&var2=" + auto);


			// btnRefresh.setText("test");

			break;

		}

	}

	// тут начинаем асинк таск для отправления данных регистрации на сервер

	class RequestRegistration extends AsyncTask<String, String, String> {

		private final String Tag = null;

		@Override
		protected String doInBackground(String... params) {

			DefaultHttpClient client = new DefaultHttpClient(); // создаем
			// клиента
			try {
				HttpResponse response = client.execute(new HttpGet(params[0])); // пытаемся
				// выполнить
				// GET
				// запрос
				HttpEntity httpEntity = response.getEntity(); // получаем
				// ответ
				// (содержимое
				// страницы
				// вывода)
				line = EntityUtils.toString(httpEntity, "UTF-8");
				Log.d(Tag, line);

			} catch (Exception e) {
				Log.i("Request exception", "Exception: " + e.getMessage()); // Oops
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {

			dialog.dismiss();

			// тут делаем кнопку нажатой после получения данных с сервера
			// btnRefresh.setText("Получено" + msg.what);
			btnRegistration.setEnabled(false);

			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {

			dialog = new ProgressDialog(Registration.this);
			dialog.setMessage("Регистрация...");
			dialog.setIndeterminate(true);
			dialog.setCancelable(true);
			dialog.show();
			super.onPreExecute();
		}
	}

}
